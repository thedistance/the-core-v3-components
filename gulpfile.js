/**
 * Created by peterhickling on 03/04/2017.
 */

const gulp = require('gulp');
const mocha = require('gulp-mocha');
const babel = require("gulp-babel");

gulp.task('test', ['compile'], () => {
  return gulp.src('test/**/*.js', {read: false})
    .pipe(mocha({reporter: 'list', compilers: 'js:babel-core/register', require: "./test/setup.js"}));
});

gulp.task('compile', () =>
  gulp.src('src/**')
    .pipe(babel({
      "presets" : ['es2015', 'stage-2', 'react'],
      "plugins": ['transform-class-properties', 'transform-decorators-legacy']
    }))
    .pipe(gulp.dest('target'))
);