import React from 'react';

import { restoreSession } from '../';
import CoreLogin from './core-login';

class CoreLoginContainer extends React.Component {
  componentDidMount() {
    this.props.dispatch(restoreSession(this.props.models));
  }

  getChildContext() {
    return {
      currentUser: this.props.currentUser,
    };
  }

  render() {
    if (this.props.currentUser) {
      if (!this.props.children) {
        return null
      } else if (React.isValidElement(this.props.children)) {
        return this.props.children;
      } else {
        return <span>{this.props.children}</span>;
      }
    } else {
      return <CoreLogin onSubmit={this.props.login} flash={this.props.flash}/>;
    }
  }
}

CoreLoginContainer.propTypes = {
  login: React.PropTypes.func.isRequired,
  currentUser: React.PropTypes.object,
  flash: React.PropTypes.string,
  models: React.PropTypes.object.isRequired,
};

CoreLoginContainer.childContextTypes = {
  currentUser: React.PropTypes.object,
};

export default CoreLoginContainer;
