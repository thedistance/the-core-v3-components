//React Component
//  Core Login Box for uname & passwd input

import React from 'react';
import { Link } from 'react-router';

import {
  Grid, Row, Col,
  PanelContainer, Panel, PanelBody, Alert,
  Form, FormGroup, InputGroup, FormControl,
  Icon,
  Button,
} from '@sketchpixy/rubix';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: props.username || '',
      password: '',
      titleMessage: props.titleMessage || "Sign in to The Core v3",
    };
  }

  componentDidMount() {
    const el = document.querySelector('html');
    if (el.classList)
      el.classList.add('authentication');
    else
      el.className += ' authentication';
  }

  componentWillUnmount() {
    const el = document.querySelector('html');
    if (el.classList)
      el.classList.remove('authentication');
    else
      el.className = el.className.replace(new RegExp('(^|\\b)authentication(\\b|$)', 'gi'), ' ');
  }

  /*
  onUsernameChange = (event) => {
    this.setState({username: event.target.value});
  }

  onPasswordChange = (event) => {
    this.setState({password: event.target.value});
  }
  */

  //Handles ANY onChange events, 
  //  uses input id property (e.g.: <div id="foo">) to identify event target
  handleChange = (event) => {
    switch(event.target.id){
      case 'username':
        this.setState({...this.state, username: event.target.value});
        break;
      case 'password':
        this.setState({...this.state, password: event.target.value});
    } 
  }

  onSubmit = () => {
    this.props.onSubmit(this.state.username, this.state.password);
  }
  handleSubmit = (e) => {
    //Checks is the event was triggered by a keypress, and continues only if the key was ENTER
    //  Also continues if event was not a keypress
    if((e.key && e.key == "Enter") || e.key == null) 
      this.props.onSubmit(this.state.username, this.state.password);
  }

  render() {
    return (
      <div id='auth-container' className='login'>
        <div id='auth-row'>
          <div id='auth-cell'>
            <Grid>
              <Row>
                <Col sm={4} smOffset={4} xs={10} xsOffset={1} collapseLeft collapseRight>
                  <PanelContainer controls={false}>
                    <Panel>
                      <PanelBody style={{padding: 0}}>
                        <div className='text-center bg-darkblue fg-white'>
                          <h3 style={{margin: 0, padding: 25}}>{this.state.titleMessage}</h3>
                        </div>
                        <div>
                          {this.props.flash &&
                            <Alert bsStyle="warning">{this.props.flash}</Alert>
                          }
                          <div style={{padding: 25, paddingTop: 0, paddingBottom: 0, margin: 'auto', marginBottom: 25, marginTop: 25}}>
                            <Form>
                              <FormGroup controlId='username'>
                                <InputGroup bsSize='large'>
                                  <InputGroup.Addon>
                                    <Icon glyph='icon-fontello-mail' />
                                  </InputGroup.Addon>
                                  <FormControl autoFocus
                                    type='text'
                                    value={this.state.username}
                                    onChange={this.handleChange}
                                    className='border-focus-blue'
                                    placeholder='username' />
                                </InputGroup>
                              </FormGroup>
                              <FormGroup controlId='password'>
                                <InputGroup bsSize='large'>
                                  <InputGroup.Addon>
                                    <Icon glyph='icon-fontello-key' />
                                  </InputGroup.Addon>
                                  <FormControl
                                    type='password'
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    onKeyPress={this.handleSubmit}
                                    className='border-focus-blue'
                                    placeholder='password' />
                                </InputGroup>
                              </FormGroup>
                              <FormGroup>
                                <Grid>
                                  <Row>
                                    <Col xs={12} collapseLeft collapseRight className='text-right'>
                                      <Button outlined lg bsStyle='blue'
                                        onClick={this.handleSubmit}
                                      >Login</Button>
                                    </Col>
                                  </Row>
                                </Grid>
                              </FormGroup>
                            </Form>
                          </div>
                        </div>
                      </PanelBody>
                    </Panel>
                  </PanelContainer>
                </Col>
              </Row>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  flash: React.PropTypes.string,
};

export default Login;
