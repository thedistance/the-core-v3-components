import React from 'react';

import {
  PanelContainer, Panel, PanelHeader, PanelBody,
  Grid, Row, Col,
} from '@sketchpixy/rubix';

const CorePanel = (props) => (
  <PanelContainer>
    <Panel>
      <PanelHeader className='bg-blue'>
        <Grid>
          <Row>
            <Col xs={6} className='fg-white'>
              <h3>{ props.title }</h3>
            </Col>
            <Col xs={6} className='fg-white' style={{ textAlign: 'right' }}>
            </Col>
          </Row>
        </Grid>
      </PanelHeader>
      <PanelBody>
        <Grid>
          <Row>
            <Col xs={12}>
              { props.children }
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </Panel>
  </PanelContainer>
);

CorePanel.propTypes = {
  title: React.PropTypes.string.isRequired,
  children: React.PropTypes.node.isRequired,
};

export default CorePanel;
