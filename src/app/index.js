import Parse from 'parse';

import CoreLoginContainer from './components/core-login-container';
import CoreLogin from './components/core-login';
import CorePanel from './components/core-panel';

const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const ASYNC_START = 'ASYNC_START';
const ASYNC_STOP = 'ASYNC_STOP';
const FLASH = 'FLASH';
const CLEAR_FLASH = 'CLEAR_FLASH';

const initialState = {
  asyncActive: false,
  currentUser: null,
  flash: null,
};

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case LOGIN:
      return {...state,
      currentUser: action.user,
    };
    case LOGOUT:
      return {...state,
      currentUser: null,
    };
    case ASYNC_START:
      return {...state, asyncAction: true};
    case ASYNC_STOP:
      return {...state, asyncAction: false};
    case FLASH:
      return {...state, flash: action.message};
    case CLEAR_FLASH:
      return {...state, flash: null};
  }
  return state;
}

const loadRoles = user => {
  if (user) {
    const query = new Parse.Query(Parse.Role);
    query.equalTo('users', user);
    return query.find().then(roles => {
      user['roles'] = roles;
      return user;
    });
  }
}

/*
 * Dispatched when mounting the CoreLoginContainer, which appears high in
 * the component tree. Restores the user's session from the browser's
 * persistent storage and then runs our hooks to load any Roles that the
 * user is a member of.
 */
export function restoreSession(models) {
  const access = models.auth.access;

  return (dispatch) => {
    access.restoreSession().then(loadRoles).then(user => {
      if (user) {
        dispatch(loggedIn(user));
      }
    });
  };
}

export function login(username, password, models) {
  const access = models.auth.access;

  return (dispatch) => {
    dispatch(clearFlash());
    dispatch(asyncStart());
    access.login(username, password).then(loadRoles).then(user => {
        console.log("Logged in as " + user.get('username') + ".");
        dispatch(loggedIn(user));
        dispatch(asyncStop());
      },
      error => {
        if (error.code == Parse.Error.OBJECT_NOT_FOUND ||
            error.code == Parse.Error.USERNAME_MISSING) {
          dispatch(flash("Incorrect username or password."));
        } else {
          console.log("Error: " + error.code + " " + error.message);
        }
        dispatch(asyncStop());
      }
    );
  };
}

export function loggedIn(user) {
  return {
    type: LOGIN,
    user: {
      parseObject: user,
      username: user.get('username'),
      firstName: user.get('first_name'),
      roles: user['roles'],
    },
  };
}

export function logout(models) {
  const access = models.auth.access;

  return (dispatch) => {
    dispatch(asyncStart());
    access.logout().then(() => {
      console.log("Logged out");
      dispatch(loggedOut());
      dispatch(asyncStop());
    });
  };
}

export function loggedOut() {
  return { type: LOGOUT };
}

export function asyncStart() {
  return { type: ASYNC_START };
}

export function asyncStop() {
  return { type: ASYNC_STOP };
}

export function flash(message) {
  return { type: FLASH, message };
}

export function clearFlash() {
  return { type: CLEAR_FLASH };
}

export const components = {
  CoreLoginContainer,
  CoreLogin,
  CorePanel,
};
