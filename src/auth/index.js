import React from 'react';

export const RequireRoles = requiredRoles => {
  const component = (props, context) => {
    if (
      context.currentUser &&
      context.currentUser.roles.some(r => requiredRoles.includes(r.get('code')))
    ) {
      return props.children;
    } else {
      return null;
    }
  };

  component.propTypes = {
    children: React.PropTypes.node.isRequired,
  };

  component.contextTypes = {
    currentUser: React.PropTypes.object,
  };

  return component;
};
