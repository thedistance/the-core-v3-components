import {asyncStart, asyncStop} from "../app";

const COLLECTION_CHANGED = 'COLLECTION_CHANGED';
const COLLECTION_LOADED = 'COLLECTION_LOADED';
const COLLECTION_COUNTED = 'COLLECTION_COUNTED';
const COLLECTION_FILTER = 'COLLECTION_FILTER';
const COLLECTION_PAGE = 'COLLECTION_PAGE';
const COLLECTION_SORT = 'COLLECTION_SORT';

const initialFinderOptions = {
  term: '',
  page: 1,
  sort: {
    column: '',
    ascending: true,
  },
};

const initialState = {
  entityName: '',
  collection: [],
  finderOptions: initialFinderOptions,
  needsInitialLoad: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case COLLECTION_CHANGED:
      return {
        ...state,
        entityName: action.entity,
        collection: [],
        count: 0,
        finderOptions: initialFinderOptions,
        needsInitialLoad: true,
      };
    case COLLECTION_LOADED:
      return {...state, collection: action.collection, needsInitialLoad: false};
    case COLLECTION_COUNTED:
      if (action.count != state.count) {
        return {...state, count: action.count, finderOptions: {
          ...state.finderOptions,
          page: 1,
        }};
      } else {
        return {...state, count: action.count};
      }
    case COLLECTION_FILTER:
      return {...state, finderOptions: {...state.finderOptions, term: action.term}};
    case COLLECTION_PAGE:
      let page = state.finderOptions.page;
      if (action.next) {
        page = page+1;
      } else if (action.previous) {
        page = page-1;
      } else if (action.page) {
        page = action.page;
      }
      return {...state, finderOptions: {...state.finderOptions, page}};
    case COLLECTION_SORT:
      if (state.finderOptions.sort.column === action.column) {
        return {...state, finderOptions: {
          ...state.finderOptions,
          sort: {
            column: action.column,
            ascending: !state.finderOptions.sort.ascending,
          },
        }};
      } else {
        return {...state, finderOptions: {
          ...state.finderOptions,
          sort: {
            column: action.column,
            ascending: initialFinderOptions.sort.ascending,
          },
        }};
      }
  }
  return state;
}

export function loadCollection(entity, models, finderOptions) {
  const model = models.get(entity);
  const access = model.access;
  const meta = model.meta;
  return (dispatch) => {
    dispatch(asyncStart());

    access.findAll({
      success: function (results) {
        console.log(`Successfully retrieved ${results.length} ${meta.plural}.`);
        let promises = [Promise.resolve()];
        for (let i = 0; i < meta.edit.length; i++) {
          if (meta.edit[i].type === "relation") {
            for (let j = 0; j < results.length; j++) {
              let relation = results[j].relation(meta.edit[i].key);
              promises.push(relation.query().find(function (objects) {
                results[j][meta.edit[i].key] = objects;
                console.log("Successfully retrieved relations for: " + meta.edit[i].key);
              }));
            }
          }
        }
        Promise.all(promises).then(function () {
          dispatch(collectionLoaded(results));
          dispatch(asyncStop());
        });
      },
      counted: function (count) {
        console.log(`${count} matching ${meta.plural}.`);
        dispatch(collectionCounted(count));
      },
      error: function (error) {
        console.log("Error: " + error.code + " " + error.message);
        dispatch(asyncStop());
      },
      finderOptions
    });
  };
}

export function collectionChangeObserver(store) {
  let willHandleInitialLoad = true;
  let currentEntityName;
  let currentFinderOptions;

  const selectEntityName = (state) => {
    return state.collection.entityName;
  }

  const selectFinderOptions = (state) => {
    return state.collection.finderOptions;
  }

  return function() {
    const state = store.getState().entitiesState;
    let previousEntityName = currentEntityName;
    currentEntityName = selectEntityName(state);
    let previousFinderOptions = currentFinderOptions;
    currentFinderOptions = selectFinderOptions(state);
    const needsInitialLoad = state.collection.needsInitialLoad;
    const doInitialLoad = willHandleInitialLoad && needsInitialLoad;

    if (!currentEntityName) {
      return;
    }

    willHandleInitialLoad = !needsInitialLoad;
    if (currentEntityName !== previousEntityName
      || previousFinderOptions !== currentFinderOptions
      || doInitialLoad) {
      store.dispatch(loadCollection(
        currentEntityName,
        state.models,
        currentFinderOptions
      ));
    }
  }
}

export function collectionChanged(entity) {
  return {
    type: COLLECTION_CHANGED,
    entity,
  };
}

export function collectionLoaded(collection) {
  return {
    type: COLLECTION_LOADED,
    collection,
  };
}

export function collectionCounted(count) {
  return {
    type: COLLECTION_COUNTED,
    count,
  };
}

export function filterCollection(term) {
  return {
    type: COLLECTION_FILTER,
    term,
  };
}

export function nextPage() {
  return {
    type: COLLECTION_PAGE,
    next: true,
  };
}

export function previousPage() {
  return {
    type: COLLECTION_PAGE,
    previous: true,
  };
}

export function gotoPage(page) {
  return {
    type: COLLECTION_PAGE,
    page,
  };
}

export function sortCollection(column) {
  return {
    type: COLLECTION_SORT,
    column,
  };
}
