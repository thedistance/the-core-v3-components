/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-21T10:00:34+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-09-11T11:02:08+01:00
 * @Copyright: The Distance
 */

import React from 'react';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import CoreControl from './core-control';

class CoreArrayControl extends CoreControl {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
    this.delimiter = props.delimiter || ',';
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const value = event.target.value.split(this.delimiter);

    this.validate(value);
    this.setState({value});
    this.props.onChange(this.props.meta.key, value);
  }

  render() {
    let {value} = this.state;

    if (value) value = value.join(this.delimiter);

    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <FormControl
          type={'text'}
          value={value}
          placeholder={this.props.meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          {...this.props.additionalAttributes}
        />
        <FormControlFeedback />
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreArrayControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.array,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
  delimiter: React.PropTypes.string,
};

export default CoreArrayControl;
