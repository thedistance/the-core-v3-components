/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-21T10:04:26+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-09-11T11:02:34+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CoreArrayControl from './core-array-control';

storiesOf('CoreArrayControl', module)
  .add('with a control label', () => (
    <CoreArrayControl
      value={['foo', 'bar']}
      meta={{key: 'keywords', label: 'Keywords'}}
      onChange={action('[0] field changed [1] new value')}
    />
  ))
  .add('with a custom delimiter', () => (
    <CoreArrayControl
      value={['foo', 'bar']}
      delimiter={';'}
      meta={{key: 'keywords', label: 'Keywords'}}
      onChange={action('[0] field changed [1] new value')}
    />
  ))
  .add('empty value', () => (
    <CoreArrayControl
      meta={{key: 'keywords', label: 'Keywords'}}
      onChange={action('[0] field changed [1] new value')}
    />
  ))
  ;
