import React from "react";
import {ControlLabel, FormControl, FormGroup, HelpBlock} from "@sketchpixy/rubix";
import CoreControl from "./core-control";

const NULL = "__NULL__";

class CoreBooleanControl extends CoreControl {
    componentDidMount() {
        this.setState({
            value: this.props.value
        });
    }

    handleChange = (event) => {
        let value = event.target.value;

        if (value === "true") {
            value = true;
        } else {
            value = false;
        }

        this.validate(value);
        this.setState({value: event.target.value});
        this.props.onChange(this.props.meta.key, value);
    };

    optionCollection() {
        return [true, false];
    }

    render() {
        return (
            <FormGroup
                controlId={this.props.id}
                validationState={this.validationState()}
            >
                {
                    this.props.meta.label &&
                    <ControlLabel>{this.props.meta.label}</ControlLabel>
                }
                <FormControl
                    componentClass="select"
                    value={this.state.value}
                    placeholder={this.props.meta.placeHolder}
                    onChange={this.handleChange}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                >
                    {
                        this.optionCollection().map(o =>
                            <option key={o} value={o}>{o.toString()}</option>
                        )
                    }
                </FormControl>
                {
                    this.props.meta.help &&
                    <HelpBlock>{this.props.meta.help}</HelpBlock>
                }
            </FormGroup>
        );
    }
}

CoreBooleanControl.propTypes = {
    id: React.PropTypes.string.isRequired,
    value: React.PropTypes.any,
    loadModelSummary: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
    meta: React.PropTypes.object.isRequired,
    models: React.PropTypes.object.isRequired,
};

export default CoreBooleanControl;
