import React from "react";
import Parse from "parse";
import {
  FormGroup,
  ControlLabel,
  Checkbox,
  HelpBlock
} from "@sketchpixy/rubix";
import CoreControl from "./core-control";

class CoreCheckboxesControl extends CoreControl {
  value(props) {
    if (props.value instanceof Parse.Object) {
      return [props.value];
    } else {
      return props.value;
    }
  }

  handleChange = event => {
    const models = this.props.models;
    const options = this.props.meta.options;
    const target = event.target;
    const checked = target.checked;
    const id = target.name;
    const sourceObject = this.sourceCollection(models, options).find(
      o => o.id == id
    );
    let value = this.state.value;
    if (checked) {
      if (!value.find(o => sourceObject.equals(o))) {
        value = value.concat(sourceObject);
      }
    } else {
      value = value.filter(o => !sourceObject.equals(o));
    }
    this.setState({ value });
    this.props.onChange(this.props.meta.key, value);
  };

  sourceCollection = (models, options) =>
    models.get(options.source).modelSummary || [];

  optionCollection() {
    const models = this.props.models;
    const options = this.props.meta.options;

    return this.sourceCollection(models, options).map(o => ({
      id: o.id,
      name: o.get(this.props.meta.value),
      checked: this.state.value.some(v => o.equals(v))
    }));
  }

  render() {
    const meta = this.props.meta;
    const options = meta.options;

    return (
      <FormGroup controlId={this.props.id}>
        {meta.label && <ControlLabel>{meta.label}</ControlLabel>}
        {this.optionCollection().map(o =>
          <Checkbox
            key={o.id}
            name={o.id}
            checked={o.checked}
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
          >
            {o.name}
          </Checkbox>
        )}
        {meta.help && <HelpBlock>{meta.help}</HelpBlock>}
      </FormGroup>
    );
  }
}

CoreCheckboxesControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.any,
  loadModelSummary: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
  models: React.PropTypes.object.isRequired
};

export default CoreCheckboxesControl;
