import React from 'react';

class CoreControl extends React.Component {
  constructor(props) {
    super(props);

    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);

    let value;

    if (this.value) {
      value = this.value(props);
    } else {
      value = props.value || '';
    }
    this.state = {
      value,
      visited: false,
      valid: null,
    };
  }

  handleFocus(event) {
    this.setState({
      visited: true,
    });
  }

  handleBlur(event) {
    this.validate(this.state.value);
  }

  validate(v) {
    this.setState({
      valid: this.isValid(v),
    });
  }

  isValid(v) {
    const validate = this.props.meta.validate;
    if (v instanceof String)
      v = v.trim();

    if (this.state.visited) {
      if (validate) {
        if (Array.isArray(validate)) {
          return validate.every(f => (f(v)));
        } else if (validate instanceof Function) {
          return validate(v);
        } else {
          console.log(`Unsupported validate value: ${validate}`);
          return true;
        }
      } else {
        return true;
      }
    } else {
      return null;
    }
  }

  validationState(field = '') {
    if (this.state.valid == null) {
      return null;
    }
    let valid;
    if (typeof this.state.valid === 'boolean') {
      valid = this.state.valid;
    } else if (typeof this.state.valid === 'object') {
      valid = this.state.valid[field];
    }
    return valid ? 'success' : 'error';
  }
}

export default CoreControl;
