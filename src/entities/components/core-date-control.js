/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-16T14:06:58+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-04-03T14:01:06+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import 'react-datepicker/dist/react-datepicker.css';

import CoreControl from './core-control';

class CoreDateControl extends CoreControl {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value ? moment.utc(props.value) : moment.utc(),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    this.validate(value);
    this.setState({value: value});
    this.props.onChange(this.props.meta.key, value.startOf('day').toDate());
  }

  renderControlLabel() {
    if (!this.props.meta.label) {
      return null;
    }
    return <ControlLabel>{this.props.meta.label}</ControlLabel>;
  }

  renderHelp() {
    if (!this.props.meta.help) {
      return null;
    }
    return <HelpBlock>{this.props.meta.help}</HelpBlock>;
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {this.renderControlLabel()}
        <DatePicker
          selected={this.state.value}
          onChange={this.handleChange}
          locale={'en-gb'}
        />
        <FormControlFeedback />
        {this.renderHelp()}
      </FormGroup>
    );
  }

  isValid(value) {
    return moment(value).isValid();
  }
}

CoreDateControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
  value: React.PropTypes.instanceOf(Date),
  meta: React.PropTypes.object.isRequired,
};

export default CoreDateControl;
