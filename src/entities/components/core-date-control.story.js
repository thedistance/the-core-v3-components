/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-16T14:30:30+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-08-16T17:27:30+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CoreDateControl from './core-date-control';

const date = new Date('2017-12-15');

storiesOf('CoreDateControl', module)
  .add('with a control label', () => (
    <CoreDateControl
      value={date}
      meta={{key: 'date', label: 'Select a date'}}
      onChange={action('[0] field changed [1] new value')}
    />
  ))
  ;
