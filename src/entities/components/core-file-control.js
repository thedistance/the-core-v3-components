import React from 'react';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import { Parse } from '../../models';

import CoreControl from './core-control';

class CoreFileControl extends CoreControl {
  handleChange = (event) => {
    const control = event.target;

    if (control.files.length > 0) {
      const file = control.files[0];

      const parseFile = new Parse.File(file.name, file);
      this.setState({value: parseFile});
      this.props.onChange(this.props.meta.key, parseFile);
    }
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        {
          this.state.value &&
            <p>Current content:{' '}
              <a href={this.state.value.url()} target="_blank">{this.state.value.name()}</a>
            </p>
        }
        <FormControl
          type="file"
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        />
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreFileControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.object,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
  models: React.PropTypes.object.isRequired,
};

export default CoreFileControl;
