import React from 'react';

import {
  Well, Grid, Row, Col,
} from '@sketchpixy/rubix';

import CoreFilterControl from './core-filter-control';
import CorePaginationControl from './core-pagination-control';

class CoreFilterAndPaginationBar extends React.Component {
  render() {
    return (
      <Well>
        <Grid>
          <Row className='row-no-padding'>
            <Col xs={6}><CoreFilterControl {...this.props} /></Col>
            <Col xs={6} style={{
              textAlign: 'right',
              lineHeight: '0.5',
            }}>
              <CorePaginationControl {...this.props} />
            </Col>
          </Row>
        </Grid>
      </Well>
    );
  }
};

export default CoreFilterAndPaginationBar;
