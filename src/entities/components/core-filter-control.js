import React from 'react';

import {
  Form, FormGroup, ControlLabel, FormControl,
} from '@sketchpixy/rubix';

import { filterCollection } from '../collection';
import { loadModelSummary } from '../models';

const NULL = "__NULL__";

class CoreFilterControl extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    };
  }

  componentWillMount() {
    this.props.dispatch(loadModelSummary(this.options().source, this.props.models));
  }

  options = () => {
    return {
      label: 'Filter',
      ...this.props.options,
    };
  }

  search = (event) => {
    let value = event.target.value;

    this.setState({ value });
    if (event.target.value === NULL) {
      value = '';
    }
    this.props.dispatch(filterCollection(value));
  }

  submit = (event) => {
    event.preventDefault();
  }

  optionCollection = () => {
    const models = this.props.models;
    const options = this.options();
    const optionModel = models.get(options.source);
    const modelSummary = optionModel.modelSummary || [];
    const collection = modelSummary.map(o => ({id: o.id, name: o.get('name')}));
    collection.unshift({
      id: NULL,
      name: 'All',
    });
    return collection;
  }

  render() {
    const options = this.options();

    return (
      <Form inline onSubmit={this.submit} autoComplete="off">
        <FormGroup controlId="searchBarTerm">
          <ControlLabel>{options.label}</ControlLabel>
          {' '}
          <FormControl
            componentClass="select"
            value={this.state.value}
            onChange={this.search}
          >
            {
              this.optionCollection().map(o =>
                <option key={o.id} value={o.id}>{o.name}</option>
              )
            }
          </FormControl>
      	</FormGroup>
      </Form>
    );
  }
};

export default CoreFilterControl;
