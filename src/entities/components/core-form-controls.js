import React from 'react';

import {Button, ButtonToolbar, Col, Form, Grid, Row} from '@sketchpixy/rubix';
import {LinkContainer} from 'react-router-bootstrap';

import CoreDateControl from './core-date-control';
import CoreTextControl from './core-text-control';
import CoreTextAreaControl from './core-text-area-control';
import CoreSelectControl from './core-select-control';
import CoreCheckboxesControl from './core-checkboxes-control';
import CoreFileControl from './core-file-control';
import CoreMultiSelectRelationControl from './core-multi-select-relation-control';
import CoreBooleanControl from './core-boolean-control';
import CoreGeoPointControl from './core-geo-point-control';
import CoreNumberControl from './core-number-control';
import CoreArrayControl from './core-array-control';

const controls = {
  array: CoreArrayControl,
  date: CoreDateControl,
  text: CoreTextControl,
  textarea: CoreTextAreaControl,
  password: CoreTextControl,
  select: CoreSelectControl,
  checkboxes: CoreCheckboxesControl,
  file: CoreFileControl,
  relation: CoreMultiSelectRelationControl,
  boolean: CoreBooleanControl,
  geopoint: CoreGeoPointControl,
  number: CoreNumberControl,
};

class CoreFormControls extends React.Component {
  passRelationIntoRelationControl = meta => {
    if (meta.type === 'relation') {
      delete this.props.instance[meta.key];
      return this.props.instance.relation(meta.key);
    } else {
      return false;
    }
  };

  metaToControl = meta => {
    const instance = this.props.instance;
    let value;

    if (instance) {
      if (instance.hasOwnProperty(meta.key)) {
        value = instance[meta.key];
      } else {
        value = instance.get(meta.key);
      }
    }

    const component = meta.component || controls[meta.type] || CoreTextControl;

    return {
      relation: this.passRelationIntoRelationControl(meta),
      component,
      value,
      meta,
    };
  };

  cancelOrDeleteButton = () => {
    if (this.props.onDelete) {
      return (
        <Button bsStyle="danger" onClick={this.props.onDelete}>
          Delete
        </Button>
      );
    } else {
      return (
        <LinkContainer to={{pathname: this.props.cancelPathname}}>
          <Button bsStyle="danger">Cancel</Button>
        </LinkContainer>
      );
    }
  };

  render() {
    if (!this.props.instance) return <div />;

    const {
      loadModelSummary,
      onChange,
      models,
      instance,
    } = this.props;

    return (
      <Form autoComplete='off'>
        {this.props.columns
          .map(this.metaToControl)
          .map(control =>
            <control.component
              key={control.meta.key}
              id={control.meta.key}
              value={control.value}
              meta={control.meta}
              relation={control.relation}
              loadModelSummary={loadModelSummary}
              onChange={onChange}
              models={models}
              instance={instance}
            />,
          )}
        <ButtonToolbar style={{marginBottom: '1em'}}>
          <Grid>
            <Row>
              <Col xs={6}>
                {this.cancelOrDeleteButton()}
              </Col>
              <Col xs={6} style={{textAlign: 'right'}}>
                <Button bsStyle="primary" onClick={this.props.onSubmit}>
                  Save
                </Button>
              </Col>
            </Row>
          </Grid>
        </ButtonToolbar>
      </Form>
    );
  }
}

CoreFormControls.propTypes = {
  instance: React.PropTypes.object,
  loadModelSummary: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  onSubmit: React.PropTypes.func,
  onDelete: React.PropTypes.func,
  cancelPathname: React.PropTypes.string,
  columns: React.PropTypes.array,
  models: React.PropTypes.object.isRequired,
};

export default CoreFormControls;
