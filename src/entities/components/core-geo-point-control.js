import React from 'react';
import Parse from 'parse';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock,
  Grid, Row, Col,
} from '@sketchpixy/rubix';

import CoreControl from './core-control';
import {models} from '../../models';

const clip = (v, range) => {
  let r = v;

  if (v < -range) {
    r = -range;
  } else if (v > range) {
    r = range;
  }
  return r;
};

const buildGeoPoint = (value) => {
  const latitude = clip(parseFloat(value.latitude) || 0, 90);
  const longitude = clip(parseFloat(value.longitude) || 0, 180);
  return new Parse.GeoPoint(latitude, longitude);
};

const Number = (props) => (
  <FormGroup
    controlId={props.id}
    validationState={props.valid}
  >
    <FormControl
      type="text"
      value={props.value}
      placeholder={props.label}
      onChange={props.onChange}
    />
    <FormControlFeedback />
  </FormGroup>
);

class CoreGeoPointControl extends CoreControl {
  value(props) {
    if (props.value) {
      return {
        latitude: props.value.latitude,
        longitude: props.value.longitude,
      };
    } else {
      return {
        latitude: '',
        longitude: '',
      }
    }
  }

  handleChange = (event) => {
    const key = event.target.id;
    const latOrLong = event.target.value;
    let value = this.state.value;

    if (key.endsWith('.latitude')) {
      value = {...value, latitude: latOrLong};
    } else if (key.endsWith('.longitude')) {
      value = {...value, longitude: latOrLong};
    }
    this.validate(value);
    this.setState({value});

    const geoPoint = buildGeoPoint(value);
    this.props.onChange(this.props.meta.key, geoPoint);
  }

  isValid(v) {
    return {
      latitude: models.float(v.latitude),
      longitude: models.float(v.longitude)
    };
  }

  render() {
    return (
      <div>
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <Grid>
          <Row className='row-no-padding'>
            <Col md={6}>
              <Number
                id={`${this.props.id}.latitude`}
                value={this.state.value.latitude}
                label='Latitude'
                onChange={this.handleChange}
                valid={this.validationState('latitude')}
              />
            </Col>
            <Col md={6}>
              <Number
                id={`${this.props.id}.longitude`}
                value={this.state.value.longitude}
                label='Longitude'
                onChange={this.handleChange}
                valid={this.validationState('longitude')}
              />
            </Col>
          </Row>
        </Grid>
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </div>
    );
  }
}

CoreGeoPointControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.object,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
};

export default CoreGeoPointControl;
