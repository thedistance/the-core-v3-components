import React from 'react';
import { Link } from 'react-router';
import { Glyphicon } from '@sketchpixy/rubix';

const CoreHeaderCell = ({
  children,
  id,
  sortable,
  finderOptions,
  sortCollection,
}) => {
  const active = finderOptions.sort.column === id;
  const style = sortable ? { cursor: 'pointer' } : {};

  return (
    <th style={style} onClick={() => sortable && sortCollection(id)}>
      {children}
      {sortable && ' '}
      {sortable &&
        (active
          ? finderOptions.sort.ascending
            ? <Glyphicon glyph="sort-by-alphabet" />
            : <Glyphicon glyph="sort-by-alphabet-alt" />
          : <Glyphicon glyph="sort" />)}
    </th>
  );
};

const CoreHeader = props =>
  <thead>
    <tr>
      {props.columns.map(({ id, label, sortable }) =>
        <CoreHeaderCell
          key={id}
          id={id}
          sortable={sortable}
          finderOptions={props.finderOptions}
          sortCollection={props.sortCollection}
        >
          {label}
        </CoreHeaderCell>,
      )}
      <th />
    </tr>
  </thead>;

export default CoreHeader;
