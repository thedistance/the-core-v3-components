/**
 * Created by peterhickling on 14/03/2017.
 */
import React from "react";
import Parse from "parse";
import {FormGroup, ControlLabel, FormControl, HelpBlock} from "@sketchpixy/rubix";
import CoreControl from "./core-control";

const NULL = "__NULL__";

class CoreMultiSelectRelationControl extends CoreControl {
  constructor(props) {
    super(props);
    this.state = {
      relation: []
    };
  }

  componentDidMount() {
    let selected = [];
    let that = this;

    this.props.relation.query().find().then(function (relationArray) {
      for (let i = 0; i < relationArray.length; i++) {
        selected.push(relationArray[i].id);
      }
      that.setState({
        value: selected
      });
      that.setState({
        relation: relationArray
      })
    });
  }

  value(props) {
    if (props.value instanceof Parse.Object) {
      return props.value.id;
    } else {
      return props.value;
    }
  }

  handleChange = (event) => {
    let selected = [];
    let selectedParseObjects = new Map();
    let newRelations = [];
    let previousRelations = this.state.relation.slice();

    for (let i = 0; i < event.target.options.length; i++) {
      if (event.target.options[i].selected) {
        let id = event.target.options[i].value;
        let option;
        selected.push(event.target.options[i].value);
        if (id == NULL) {
          option = null;
        } else {
          option = this.optionCollection().parseCollection.find(o => o.id == id);
          selectedParseObjects.set(option.id, option);
          this.validate(option);
          newRelations.push(option);
          this.props.relation.add(option);
        }
      }
    }

    for (let i = 0; i < previousRelations.length; i++) {
      if (!selectedParseObjects.has(previousRelations[i].id)) {
        this.props.relation.remove(previousRelations[i]);
      }
    }

    this.setState({value: selected, relation: newRelations});
  };

  optionCollection() {
    const models = this.props.models;
    const options = this.props.meta.options;
    let collection = [];
    let parseCollection = [];

    if (options.source) {
      parseCollection = models.get(options.source).modelSummary || [];
      collection = parseCollection.map(o => ({id: o.id, name: o.get(this.props.meta.value)}));
    } else if (options.collection) {
      collection = options.collection;
    }
    collection = collection.slice(0);
    if (options.allowBlank) {
      collection.unshift({
        id: "__null__",
        get(f) {
          return "";
        }
      });
    }
    return {dropDownCollection: collection, parseCollection: parseCollection};
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
          <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <FormControl
          componentClass="Select"
          multiple
          value={this.state.value}
          placeholder={this.props.meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        >
          {
            this.optionCollection().dropDownCollection.map(o =>
              <option key={o.id} value={o.id}>{o.name}</option>
            )
          }
        </FormControl>
        {
          this.props.meta.help &&
          <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreMultiSelectRelationControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.any,
  loadModelSummary: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
  models: React.PropTypes.object.isRequired,
};

export default CoreMultiSelectRelationControl;
