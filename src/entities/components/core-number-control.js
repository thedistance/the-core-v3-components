/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-18T14:57:33+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-08-18T16:36:35+01:00
 * @Copyright: The Distance
 */

import React from 'react';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import CoreControl from './core-control';

class CoreNumberControl extends CoreControl {
  constructor(props) {
    super(props);
    this.state = {
      value: parseInt(props.value, 10),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.validate(event.target.value);
    this.setState({value: event.target.value});
    this.props.onChange(this.props.meta.key, parseInt(event.target.value, 10));
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <FormControl
          type={'number'}
          value={this.state.value}
          placeholder={this.props.meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          {...this.props.additionalAttributes}
        />
        <FormControlFeedback />
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreNumberControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.number,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
};

export default CoreNumberControl;
