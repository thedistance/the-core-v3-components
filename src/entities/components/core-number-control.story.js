/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-08-18T15:14:55+01:00
 * @Email:  benbriggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-08-18T15:32:50+01:00
 * @Copyright: The Distance
 */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CoreNumberControl from './core-number-control';

storiesOf('CoreNumberControl', module)
  .add('with a control label', () => (
    <CoreNumberControl
      value={9001}
      meta={{key: 'number', label: 'Number'}}
      onChange={action('[0] field changed [1] new value')}
    />
  ))
  ;
