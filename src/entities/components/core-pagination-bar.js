import React from 'react';

import {
  Well,
} from '@sketchpixy/rubix';

import CorePaginationControl from './core-pagination-control';

class CorePaginationBar extends React.Component {
  render() {
    return (
      <Well>
        <CorePaginationControl {...this.props} />
      </Well>
    );
  }
};

export default CorePaginationBar;
