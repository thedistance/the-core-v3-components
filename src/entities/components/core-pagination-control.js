import React from 'react';

import {
  Pagination
} from '@sketchpixy/rubix';

import { gotoPage } from '../collection';

class CorePaginationControl extends React.Component {
  constructor(props) {
    super(props);
  }

  options = () => {
    return {
      pageSize: 100,
      maxButtons: 5,
      ...this.props.options,
    };
  }

  selectPage = (page) => {
    this.props.dispatch(gotoPage(page));
  }

  render() {
    const options = this.options();
    const pages = Math.ceil(this.props.count / options.pageSize);
    const page = this.props.finderOptions.page;

    return (
      <Pagination items={pages} activePage={page}
        prev next boundaryLinks
        maxButtons={options.maxButtons}
        onSelect={this.selectPage}
        bsSize='small' style={{margin: '0'}}
      />
    );
  }
};

export default CorePaginationControl;
