import React from "react";
import {Button} from "@sketchpixy/rubix";
import {LinkContainer} from "react-router-bootstrap";

const convertValue = (valueObj, convertToString = null) => {
  let value = valueObj.entity.get(valueObj.key);

  if (valueObj.entity[valueObj.key]) {
    value = valueObj.entity[valueObj.key];
  }

  if(value == null) {
    value = "";
  } else if (convertToString) {
    try {
      value = convertToString(value);
    } catch (e) {
      if (process.env.NODE_ENV !== 'production') {
        console.log(e);
      }
      value = "<error>";
    }
  } else if (typeof value == "object") {
    value = "<object>";
  }
  return value;
};

const cell = (t) => (
  <td key={t.id}>{convertValue(t.value, t.convertToString)}</td>
);

const CoreRow = (props) => (
  <tr>
    {
      props.columns.map(meta => ({
        ...meta,
        value: {entity: props.entity, key: meta.key},
      })).map(cell)
    }
    <td style={{textAlign: 'right'}}>
      <LinkContainer to={{pathname: `/${props.entityName}/${props.entity.id}/edit`}}>
        <Button outlined>Edit</Button>
      </LinkContainer>
    </td>
  </tr>
);

export default CoreRow;
