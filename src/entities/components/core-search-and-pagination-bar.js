import React from 'react';

import {
  Well, Grid, Row, Col,
} from '@sketchpixy/rubix';

import CoreSearchControl from './core-search-control';
import CorePaginationControl from './core-pagination-control';

class CoreSearchAndPaginationBar extends React.Component {
  render() {
    return (
      <Well>
        <Grid>
          <Row className='row-no-padding'>
            <Col xs={6}><CoreSearchControl {...this.props} /></Col>
            <Col xs={6} style={{
              textAlign: 'right',
              lineHeight: '0.5',
            }}>
              <CorePaginationControl {...this.props} />
            </Col>
          </Row>
        </Grid>
      </Well>
    );
  }
};

export default CoreSearchAndPaginationBar;
