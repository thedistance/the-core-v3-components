import React from 'react';

import {
  Well,
} from '@sketchpixy/rubix';

import CoreSearchControl from './core-search-control';

class CoreSearchBar extends React.Component {
  render() {
    return (
      <Well>
        <CoreSearchControl {...this.props} />
      </Well>
    );
  }
};

export default CoreSearchBar;
