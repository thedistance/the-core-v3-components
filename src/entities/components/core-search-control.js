import React from 'react';

import {
  Form, FormGroup, ControlLabel, FormControl,
} from '@sketchpixy/rubix';

import { filterCollection } from '../collection';

class CoreSearchControl extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    };
  }

  options = () => {
    return {
      label: 'Filter',
      placeHolder: '',
      ...this.props.options,
    };
  }

  search = (event) => {
    this.setState({value: event.target.value});
    this.props.dispatch(filterCollection(event.target.value));
  }

  submit = (event) => {
    event.preventDefault();
  }

  render() {
    const options = this.options();

    return (
      <Form inline onSubmit={this.submit} autoComplete="off">
        <FormGroup controlId="searchBarTerm">
          <ControlLabel>{options.label}</ControlLabel>
          {' '}
          <FormControl
            type="text"
            value={this.state.value}
            onChange={this.search}
            placeholder={options.placeHolder}
          />
      	</FormGroup>
      </Form>
    );
  }
};

export default CoreSearchControl;
