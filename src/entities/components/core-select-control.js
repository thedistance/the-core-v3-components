import React from "react";
import Parse from "parse";
import {FormGroup, ControlLabel, FormControl, HelpBlock} from "@sketchpixy/rubix";
import CoreControl from "./core-control";

const NULL = "__NULL__";

class CoreSelectControl extends CoreControl {
  value(props) {
    if (props.value instanceof Parse.Object) {
      return props.value.id;
    } else {
      return props.value;
    }
  }

  handleChange = (event) => {
    const id = event.target.value;
    let option;

    if (id == NULL) {
      option = null;
    } else {
      option = this.optionCollection().parseObjectsCollection.find(o => o.id == id);
      if (!(option instanceof Parse.Object)) {
        option = option.value;
      }
    }

    this.validate(option);
    this.setState({value: event.target.value});
    this.props.onChange(this.props.meta.key, option);
  }

  optionCollection() {
    const models = this.props.models;
    const options = this.props.meta.options;
    let parseCollection;
    let collection;

    if (options.source) {
      parseCollection = models.get(options.source).modelSummary || [];
      collection = parseCollection.map(o => ({id: o.id, name: o.get(this.props.meta.value)}));
    } else if (options.collection) {
      collection = options.collection;
    }
    if (options.sort) collection = collection.sort(options.sort);
    collection = collection.slice(0);
    if (options.allowBlank) {
      collection.unshift({
        id: NULL,
        get(f) {
          return "";
        }
      });
    }
    return {selectorCollection: collection, parseObjectsCollection: parseCollection};
  }

  render() {
    const meta = this.props.meta;
    const options = meta.options;

    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          meta.label &&
          <ControlLabel>{meta.label}</ControlLabel>
        }
        <FormControl
          componentClass="select"
          value={this.state.value}
          placeholder={meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        >
          {
            this.optionCollection().selectorCollection.map(o =>
              <option key={o.id} value={o.id}>{o.name}</option>
            )
          }
        </FormControl>
        {
          meta.help &&
          <HelpBlock>{meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreSelectControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.any,
  loadModelSummary: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
  models: React.PropTypes.object.isRequired,
};

export default CoreSelectControl;
