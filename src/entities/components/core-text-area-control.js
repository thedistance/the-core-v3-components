import React from 'react';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import CoreControl from './core-control';

class CoreTextAreaControl extends CoreControl {
  handleChange = (event) => {
    this.validate(event.target.value);
    this.setState({value: event.target.value});
    this.props.onChange(this.props.meta.key, event.target.value);
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <FormControl
          componentClass="textarea"
          value={this.state.value}
          placeholder={this.props.meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        />
        <FormControlFeedback />
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreTextAreaControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
};

export default CoreTextAreaControl;
