import React from 'react';

import {
  FormGroup, ControlLabel, FormControl, FormControlFeedback, HelpBlock
} from '@sketchpixy/rubix';

import CoreControl from './core-control';

class CoreTextControl extends CoreControl {
  handleChange = (event) => {
    this.validate(event.target.value);
    this.setState({value: event.target.value});
    this.props.onChange(this.props.meta.key, event.target.value);
  }

  render() {
    const password = this.props.meta.type === 'password';
    const type = password ? 'password' : 'text';

    return (
      <FormGroup
        controlId={this.props.id}
        validationState={this.validationState()}
      >
        {
          this.props.meta.label &&
            <ControlLabel>{this.props.meta.label}</ControlLabel>
        }
        <FormControl
          type={type}
          value={this.state.value}
          placeholder={this.props.meta.placeHolder}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          {...(password && {autoComplete: 'new-password'})}
        />
        <FormControlFeedback />
        {
          this.props.meta.help &&
            <HelpBlock>{this.props.meta.help}</HelpBlock>
        }
      </FormGroup>
    );
  }
}

CoreTextControl.propTypes = {
  id: React.PropTypes.string.isRequired,
  value: React.PropTypes.string,
  onChange: React.PropTypes.func.isRequired,
  meta: React.PropTypes.object.isRequired,
};

export default CoreTextControl;
