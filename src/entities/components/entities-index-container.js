import React from 'react';
import { connect } from 'react-redux';

import { collectionChanged, sortCollection } from '../collection';
import EntitiesIndex from './entities-index';
import CoreAccessDenied from './core-access-denied';

class EntitiesIndexContainer extends React.Component {
  entityName() {
    return this.props.params.entity;
  }

  collectionChanged(entityName) {
    this.props.dispatch(collectionChanged(entityName));
  }

  componentDidMount() {
    this.collectionChanged(this.entityName());
  }

  componentWillReceiveProps(nextProps) {
    const nextEntityName = nextProps.params.entity;

    if (nextEntityName != this.entityName()) {
      this.collectionChanged(nextEntityName);
    }
  }

  render() {
    if (
      this.props.models.permitted(this.entityName(), this.context.currentUser)
    ) {
      return <EntitiesIndex {...this.props} entityName={this.entityName()} />;
    } else {
      return <CoreAccessDenied />;
    }
  }
}

EntitiesIndexContainer.contextTypes = {
  currentUser: React.PropTypes.object,
};

const mapStateToProps = function(store) {
  return {
    loading: store.appState.asyncAction,
    collection: store.entitiesState.collection.collection,
    count: store.entitiesState.collection.count,
    finderOptions: store.entitiesState.collection.finderOptions,
    models: store.entitiesState.models,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sortCollection: column => {
      dispatch(sortCollection(column));
    },
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  EntitiesIndexContainer,
);
