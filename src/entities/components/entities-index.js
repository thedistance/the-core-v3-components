import React from 'react';

import {
  PanelContainer, Panel, PanelHeader, PanelBody,
  Grid, Row, Col,
  Table, Button,
} from '@sketchpixy/rubix';
import { LinkContainer } from 'react-router-bootstrap';

import CoreHeader from './core-header';
import CoreRow from './core-row';

const hasRows = (props) => (props.collection.length > 0);

const columns = (props) => {
  const meta = props.models.get(props.entityName).meta;
  let c;

  if (meta.index) {
    c = meta.index;
    for(let i = 0; i < c.length; i++) {
      if (!c[i].id) {
        c[i].id = c[i].key;
      }
    }
  } else if (props.collection.length > 0) {
    c = Object.keys(props.collection[0].attributes).map(key => ({ key, label: key }));
  } else {
    c = []
  }
  return c;
};

const EmptyIndex = (props) => {
  const meta = props.models.get(props.entityName).meta;

  if (props.loading) {
    return null;
  } else if (props.finderOptions.term !== '') {
    return (
      <p>
        No matching {meta.plural.toLowerCase()}.
      </p>
    );
  } else {
    return (
      <p>
        There are no {meta.plural.toLowerCase()} yet.
        Create one with the button above.
      </p>
    );
  }
};

const NonEmptyIndex = (props) => {
  const meta = props.models.get(props.entityName).meta;

  return (
    <Table hover responsive>
      <CoreHeader
        columns={columns(props)}
        finderOptions={props.finderOptions}
        sortCollection={props.sortCollection}
      />
      <tbody>
        {props.collection.map(entity => (
          <CoreRow key={entity.id} entity={entity} entityName={props.entityName} columns={columns(props)} />
        ))}
      </tbody>
    </Table>
  );
};

const EmptyOrNonEmptyIndex = (props) => {
  const meta = props.models.get(props.entityName).meta;

  if (hasRows(props)) {
    return <NonEmptyIndex {...props} />
  } else {
    return <EmptyIndex {...props} />
  }
};

const EntitiesIndex = (props) => {
  function sliceComponent(meta) {
    if (meta) {
      const { component, ...options } = meta;
      return [component, options];
    }
    return [null, null];
  }

  const meta = props.models.get(props.entityName).meta;
  const { indexHeaderBar, indexFooterBar } = meta;
  const [ HeaderBar, headerBarOptions ] = sliceComponent(indexHeaderBar)
  const [ FooterBar, footerBarOptions ] = sliceComponent(indexFooterBar);

  return (
    <PanelContainer className={ `entity-${props.entityName}` }>
      <Panel>
        <PanelHeader className='bg-blue'>
          <Grid>
            <Row>
              <Col xs={6} className='fg-white'>
                <h3>{meta.plural} {props.loading ? ' loading...' : ''}</h3>
              </Col>
              <Col xs={6} className='fg-white' style={{ textAlign: 'right' }}>
                <LinkContainer to={{ pathname: `/${props.entityName}/new` }}>
                  <Button bsStyle="primary">New {meta.singular}</Button>
                </LinkContainer>
              </Col>
            </Row>
          </Grid>
        </PanelHeader>
        <PanelBody>
          <Grid>
            {
              indexHeaderBar &&
              <Row>
                <Col xs={12}>
                  <HeaderBar {...props} options={headerBarOptions} />
                </Col>
              </Row>
            }
            <Row>
              <Col xs={12}>
                <EmptyOrNonEmptyIndex {...props} />
              </Col>
            </Row>
            {
              indexFooterBar &&
              <Row>
                <Col xs={12}>
                  <FooterBar {...props} options={footerBarOptions} />
                </Col>
              </Row>
            }
          </Grid>
        </PanelBody>
      </Panel>
    </PanelContainer>
  );
};

EntitiesIndex.propTypes = {
  loading: React.PropTypes.bool,
  entityName: React.PropTypes.string.isRequired,
  collection: React.PropTypes.array,
  count: React.PropTypes.number,
  finderOptions: React.PropTypes.object.isRequired,
  models: React.PropTypes.object.isRequired,
  sortCollection: React.PropTypes.func.isRequired,
};

export default EntitiesIndex;
