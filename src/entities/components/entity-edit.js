import React from 'react';

import {
  PanelContainer, Panel, PanelHeader, PanelBody,
  Grid, Row, Col,
  Button,
} from '@sketchpixy/rubix';
import {
  Form
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import Loading from './core-loading';
import CoreFormControls from './core-form-controls';

const formTitle = (meta, operation) => {
  if (operation === 'create') {
    return `New ${meta.singular}`;
  } else {
    return `Edit ${meta.singular}`;
  }
};

const columns = (props) => {
  const meta = props.models.get(props.entityName).meta;
  let c;

  if (meta.edit) {
    c = meta.edit;
  } else if (props.instance) {
    c = Object.keys(props.instance.attributes).map(key => ({ key, label: key }));
  } else {
    c = []
  }
  return c;
};

const FormOrLoading = (props) => {
  if (props.instance) {
    return (
      <CoreFormControls {...props} columns={columns(props)} />
    );
  } else {
    return (
      <Loading />
    );
  }
};

const EntityEdit = (props) => {
  const meta = props.models.get(props.entityName).meta;

  return (
  <PanelContainer className={ `entity-${props.entityName}` }>
    <Panel>
      <PanelHeader className='bg-blue'>
        <Grid>
          <Row>
            <Col xs={12} className='fg-white'>
              <h3>{props.title || formTitle(meta, props.operation)}</h3>
            </Col>
          </Row>
        </Grid>
      </PanelHeader>
      <PanelBody>
        <Grid>
          <Row>
            <Col xs={12}>
              <FormOrLoading {...props} />
            </Col>
          </Row>
        </Grid>
      </PanelBody>
    </Panel>
  </PanelContainer>
  );
};

EntityEdit.propTypes = {
  loading: React.PropTypes.bool,
  entityName: React.PropTypes.string.isRequired,
  instance: React.PropTypes.object,
  operation: React.PropTypes.string,
  loadModelSummary: React.PropTypes.func.isRequired,
  onSubmit: React.PropTypes.func,
  onDelete: React.PropTypes.func,
  cancelPathname: React.PropTypes.string,
  models: React.PropTypes.object.isRequired,
  title: React.PropTypes.string
};

export default EntityEdit;
