import React from 'react';
import { connect } from 'react-redux';

import {
  loadEdit, closeForm, fieldChange, save
} from '../instance';
import {
  loadModelSummary,
} from '../models';
import EntityEdit from './entity-edit';
import CoreAccessDenied from './core-access-denied';

class EntityNewContainer extends React.Component {
  entityName() {
    return this.props.params.entity;
  }

  componentWillMount() {
    this.props.dispatch(
      loadEdit(null, this.entityName(), this.props.models),
    );
  }

  componentWillUnmount() {
    this.props.dispatch(closeForm());
  }

  loadModelSummary = (entity) => {
    this.props.dispatch(loadModelSummary(entity, this.props.models));
  }

  onFieldChange = (key, value) => {
    this.props.dispatch(fieldChange(key, value));
  }

  onEditComplete = () => {
    this.props.dispatch(save(this.props.instance, this.entityName(), this.props.models, this.props.router));
  }

  render() {
    if (
      this.props.models.permitted(this.entityName(), this.context.currentUser)
    ) {
      return (
        <EntityEdit {...this.props}
          entityName={this.entityName()}
          loadModelSummary={this.loadModelSummary}
          onChange={this.onFieldChange}
          onSubmit={this.onEditComplete}
          cancelPathname={`/${this.props.params.entity}`}
        />
      );
    } else {
      return <CoreAccessDenied />;
    }
  }
}

EntityNewContainer.contextTypes = {
  currentUser: React.PropTypes.object,
};

const mapStateToProps = function(store) {
  return {
    loading: store.appState.asyncActive,
    instance: store.entitiesState.instance.instance,
    operation: store.entitiesState.instance.operation,
    models: store.entitiesState.models,
  };
}

export default connect(mapStateToProps)(EntityNewContainer);
