import EntitiesIndexContainer from './entities-index-container';
import EntitiesIndex from './entities-index';
import EntityNewContainer from './entity-new-container';
import EntityEditContainer from './entity-edit-container';
import EntityEdit from './entity-edit';
import CoreFormControls from './core-form-controls';
import CoreTextControls from './core-text-control';
import CoreTextAreaControls from './core-text-area-control';
import CoreGeoPointControl from './core-geo-point-control';

import CoreFilterAndPaginationBar from './core-filter-and-pagination-bar';
import CorePaginationBar from './core-pagination-bar';
import CoreSearchBar from './core-search-bar';
import CoreSearchAndPaginationBar from './core-search-and-pagination-bar';

export default {
  EntitiesIndexContainer,
  EntitiesIndex,
  EntityNewContainer,
  EntityEditContainer,
  EntityEdit,
  CoreFormControls,
  CoreTextControls,
  CoreTextAreaControls,
  CoreGeoPointControl,

  CoreFilterAndPaginationBar,
  CorePaginationBar,
  CoreSearchBar,
  CoreSearchAndPaginationBar,
};
