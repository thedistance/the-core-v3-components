import { combineReducers } from 'redux';

import collection, * as collectionActions from './collection';
import instance, * as instanceActions from './instance';
import modelsReducer, * as modelsActions from './models';
export { default as components } from './components';

function entitiesReducer(models) {
  return combineReducers({
    collection,
    instance,
    models: modelsReducer(models),
  });
}

export default entitiesReducer;

export const actions = {
  ...collectionActions,
  ...instanceActions,
  ...modelsActions,
};
