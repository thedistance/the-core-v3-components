import Parse from 'parse';

import {asyncStart, asyncStop} from "../app";
import {modelSummaryLoaded} from './models';
import {loadRelations} from './object-extensions';

const INITIALISE_EDIT = 'INITIALISE_EDIT';
const FIELD_CHANGE = 'FIELD_CHANGE';
const REVERT = 'REVERT';
const CLOSE_FORM = 'CLOSE_FORM';

const initialState = {
  instance: null,
  operation: '',
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case INITIALISE_EDIT:
      return {...state, instance: action.instance, operation: action.operation};
    case FIELD_CHANGE:
      let instance = state.instance;
      if (instance) {
        if (instance.hasOwnProperty(action.name)) {
          instance[action.name] = action.value;
        } else {
          instance.set(action.name, action.value);
        }
      }
      return {...state};
    case REVERT:
      return {...state, instance: action.instance};
    case CLOSE_FORM:
      return {...state, instance: null, operation: null};
  }
  return state;
}

export function loadEdit(id, entityName, models) {
  const model = models.get(entityName);
  const access = model.access;
  const meta = model.meta;
  const defaultValues = model.defaultValues;
  const relatedModels = models.relatedModels(entityName);

  return (dispatch) => {
    dispatch(asyncStart());

    const promises = [];
    let operation;
    if (id) {
      operation = 'update';
      promises.push(access.find(id));
    } else {
      operation = 'create';
      promises.push(Parse.Promise.resolve(access.initialiseNew(defaultValues)))
    }
    promises.push(...relatedModels.map(relatedModel => {
      const model = models.get(relatedModel);
      const access = model.access;
      const meta = model.meta;

      return access.findAll().then(results => {
        console.log(`Successfully retrieved ${results.length} ${meta.plural} for model summary.`);
        dispatch(modelSummaryLoaded(relatedModel, results));
      });
    }));
    Parse.Promise.when(promises).then(results => {
      const result = results[0];

      return loadRelations(result, meta, models).then(() => {
        dispatch(initialiseEdit(result));
      });
    },
    errors => {
      console.log(errors);
    }).always(() => {
      dispatch(asyncStop());
    });
  };
}

export function initialiseEdit(instance, operation) {
  return {
    type: INITIALISE_EDIT,
    instance,
    operation,
  };
}

export function fieldChange(name, value) {
  return {
    type: FIELD_CHANGE,
    name,
    value,
  };
}

export function revert(instance) {
  instance.revert();
  return {
    type: REVERT,
    instance,
  };
}

export function save(instance, entity, models, router) {
  const model = models.get(entity);
  const access = model.access;
  const meta = model.meta;
  return (dispatch) => {
    dispatch(asyncStart());

    access.save(instance, {
      success: function (savedInstance) {
        console.log(`Successfully saved ${meta.singular} with id ${savedInstance.id}.`);
        router.push(`/${entity}`);
        dispatch(asyncStop());
      },
      error: function (savedInstance, error) {
        console.log("Error: " + error.code + " " + error.message);
        dispatch(asyncStop());
      }
    });
  };
}

export function destroy(instance, entity, models, router) {
  const model = models.get(entity);
  const access = model.access;
  const meta = model.meta;

  return (dispatch) => {
    dispatch(asyncStart());

    access.delete(instance, {
      success: function (savedInstance) {
        console.log(`Successfully deleted ${meta.singular} with id ${instance.id}.`);
        router.push(`/${entity}`);
        dispatch(asyncStop());
      },
      error: function (savedInstance, error) {
        console.log("Error: " + error.code + " " + error.message);
        dispatch(asyncStop());
      }
    });
  };
}

export function closeForm() {
  return {
    type: CLOSE_FORM,
  };
}
