import {asyncStart, asyncStop} from "../app";

const MODEL_SUMMARY_LOADED = 'MODEL_SUMMARY_LOADED';

export default function modelsReducer(models) {
  return function reducer(state = models, action) {
    switch (action.type) {
      case MODEL_SUMMARY_LOADED:
        const model = state.get(action.entity);
        model.modelSummary = action.modelSummary;
        return {...state};
    }
    return state;
  }
}

export function loadModelSummary(entity, models) {
  const model = models.get(entity);
  const access = model.access;
  const meta = model.meta;

  return (dispatch) => {
    dispatch(asyncStart());

    access.findAll({
      success: function (results) {
        console.log(`Successfully retrieved ${results.length} ${meta.plural} for model summary.`);

        dispatch(modelSummaryLoaded(entity, results));
        dispatch(asyncStop());
      },
      error: function (error) {
        console.log("Error: " + error.code + " " + error.message);
        dispatch(asyncStop());
      }
    });
  };
}

export function modelSummaryLoaded(entity, modelSummary) {
  return {
    type: MODEL_SUMMARY_LOADED,
    entity,
    modelSummary,
  };
}
