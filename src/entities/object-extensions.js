import Parse from 'parse';

export function loadRelations(object, meta, models) {
  const newObject = object.isNew();
  const relations = meta.relations;
  let promises = [Parse.Promise.resolve()];
  if (relations) {
    for (let property in relations) {
      const relation = relations[property];
      if (relation.type === 'member') {
        const relationModel = models.get(relation.source);
        if (object.isNew()) {
          object[property] = [];
        } else {
          const relationAccess = relationModel.access;
          const query = relationAccess.buildQuery();
          query.equalTo(relation.property, object);
          promises.push(query.find().then(function (results) {
            object[property] = results;
          }));
        }
        object[`${property}#all`] = relationModel.modelSummary;
      }
    }
  }
  return Parse.Promise.when(promises).then(function() {
    return object;
  });
}
