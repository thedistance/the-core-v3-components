import appReducer, * as appActions from './app';
import entitiesReducer, {
  components as entitiesComponents,
  actions as entitiesActions,
} from './entities';
import { coreModels, models, Parse, parseInitialize, parseAccess, parseUserAccess } from './models';
import { RequireRoles } from './auth';

export const app = {
    reducer: appReducer,
    ...appActions
};

export const entities = {
  reducer: entitiesReducer,
  components: entitiesComponents,
  ...entitiesActions
};

export {
  coreModels,
  models,
  Parse,
  parseInitialize,
  parseAccess,
  parseUserAccess,

  RequireRoles,
};
