import * as auth from './parse-auth';
import Parse, { parseInitialize, parseAccess } from './parse-access';
import { parseUserAccess } from './parse-user-access';

function synthesiseModel(entityName) {
  const parseObjectName = entityName
    .replace(/^[a-z]/, m => m.toUpperCase())
    .replace(/-([a-z])/g, m => m[1].toUpperCase());

  return {
    access: parseAccess(parseObjectName),

    meta: {
      singular: parseObjectName,
      plural: parseObjectName,
    },
  };
}

const coreModels = {
  auth,

  get: function(entityName) {
    let entity = this[entityName];

    if (entity == null) {
      console.warn(
        `models.get: synthesising entity '${entityName}'. ` +
          `This is a developer convenience - create a proper model ` +
          `definition for this Parse class.`,
      );
      this[entityName] = entity = synthesiseModel(entityName);
    }
    return entity;
  },

  permitted: function(entityName, currentUser) {
    const entity = this.get(entityName);
    const requiredRoles = entity.meta.requiredRoles;

    if (requiredRoles) {
      return (
        currentUser &&
        currentUser.roles.some(r => requiredRoles.includes(r.get('code')))
      );
    } else {
      // No roles required => all permitted
      return true;
    }
  },

  relatedModels: function(entityName) {
    const entity = this.get(entityName);
    const editableProperties = entity.meta.edit || [];

    return editableProperties.reduce((a, prop) => {
      if (prop.options && prop.options.source) {
        return a.concat(prop.options.source);
      } else {
        return a;
      }
    }, []);
  },
};

const models = {
  truncate(length) {
    return function(v) {
      if (v == null) return '<null>';
      if (v.length > length) {
        return v.substring(0, length) + '...';
      } else {
        return v;
      }
    };
  },

  present(v) {
    return v.length > 0;
  },

  length(opts) {
    const { min, max } = opts;

    return function(v) {
      const len = v.length;

      if (min && max) {
        return len >= min && len <= max;
      } else if (min) {
        return len >= min;
      } else if (max) {
        return len <= max;
      }
      return true;
    };
  },

  regex(r) {
    return function(v) {
      return r.test(v);
    };
  },

  float(n) {
    return (
      typeof n === 'number' ||
      (n.indexOf('e') === -1 && !isNaN(parseFloat(n)) && isFinite(n))
    );
  },
};

export {
  coreModels,
  models,
  Parse,
  parseInitialize,
  parseAccess,
  parseUserAccess,
};
