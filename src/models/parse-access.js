import Parse from 'parse';

export default Parse;

export function parseInitialize(url, applicationId) {
  Parse.initialize(applicationId);
  Parse.serverURL = url;
}

export function parseAccess(model, extend = {}) {
  const Entity = Parse.Object.extend(model);

  return {
    buildQuery() {
      return new Parse.Query(Entity);
    },

    findAll(options = {}) {
      const {success, counted, error, finderOptions} = options;
      var query = this.buildQuery();

      if (extend.findAll) {
        query = extend.findAll(query, finderOptions);
      }
      if (counted) {
        query.count({
          success: counted,
          error,
        });
      }
      return query.find()._thenRunCallbacks({
        success,
        error,
      });
    },

    initialiseNew(defaultValues = {}) {
      return new Entity(defaultValues);
    },

    find(id, options = {}) {
      var query = new Parse.Query(Entity);

      if (extend.find) {
        query = extend.find(query);
      }
      return query.get(id)._thenRunCallbacks(options, null);
    },

    save(instance, options = {}) {
      return instance.save(null)._thenRunCallbacks(options, instance);
    },

    delete(instance, options = {}) {
      return instance.destroy()._thenRunCallbacks(options, instance);
    },
  };
}
