import Parse from 'parse';

export const access = {
  restoreSession() {
    return Parse.User.currentAsync();
  },

  login(username, password) {
    return Parse.User.logIn(username, password);
  },

  logout() {
    return Parse.User.logOut();
  }
};
