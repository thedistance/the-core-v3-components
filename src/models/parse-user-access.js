import Parse from 'parse';

export function parseUserAccess(extend = {}) {
  return {
    buildQuery() {
      return new Parse.Query(Parse.User);
    },

    findAll(options = {}) {
      const {success, counted, error, finderOptions} = options;
      var query = this.buildQuery();

      if (extend.findAll) {
        query = extend.findAll(query, finderOptions);
      }
      if (counted) {
        query.count({
          success: counted,
          error,
        });
      }
      return query.find()._thenRunCallbacks({
        success,
        error,
      });
    },

    initialiseNew() {
      return new Parse.User();
    },

    find(id, options = {}) {
      var query = new Parse.Query(Parse.User);

      if (extend.find) {
        query = extend.find(query);
      }
      return query.get(id).then(user => {
        if (extend.afterLoad) extend.afterLoad(user);
        return user;
      })._thenRunCallbacks(options, null);
    },

    save(user, options = {}) {
      if (extend.beforeSave) extend.beforeSave(user);
      return user.save(null, { useMasterKey: true }).then(function () {
        const allRoles = user['role#all'] || user['roles#all'];
        const grantedRoles = user.role || user.roles;
        let promises = [];

        if (allRoles && grantedRoles) {
          promises.push(allRoles.map(role => {
            if (grantedRoles.some(grantedRole => role.equals(grantedRole))) {
              role.getUsers().add(user);
            } else {
              role.getUsers().remove(user);
            }
            return role.save();
          }));
        }
        return Parse.Promise.when(promises).then(function () {
          return user;
        });
      })._thenRunCallbacks(options, user);
    },

    delete(user, options = {}) {
      return user.destroy({ useMasterKey: true })._thenRunCallbacks(options, user);
    },
  };
}
