/**
 * Created by peterhickling on 30/03/2017.
 */
"use strict";

import React from "react";
import {expect} from "chai";
import {shallow, mount} from "enzyme";
import CoreMultiSelectRelationControl from "../../../target/entities/components/core-multi-select-relation-control";
import {FormControl} from "@sketchpixy/rubix";
let sinon = require("sinon");

describe("Multi select relation component", () => {
  let stubLoadModelSummary = () => {
  };
  let stubOnChange = () => {
  };
  let optionOne = {
    id: 1, get: (prop) => {
      return "test1"
    }
  };
  let optionTwo = {
    id: 2, get: (prop) => {
      return "test2"
    }
  };
  let relation = {
    query: () => {
      return {
        find: () => {
          return Promise.resolve([optionOne])
        }
      }
    },
    add: () => {
    },
    remove: () => {
    }
  };
  let meta = {options: {source: true}};
  let models = {
    get: (string) => {
      return {modelSummary: [optionOne, optionTwo]}
    }
  };
  let wrapper;
  let mockSelectEvent = {
    target: {
      options: [{value: 1, selected: true},
        {value: 2}]
    }
  };

  let mockDeselectEvent = {
    target: {
      options: [{value: 1, selected: false},
        {value: 2}]
    }
  };
  let coreMultiSelect = <CoreMultiSelectRelationControl value={["1"]} loadModelSummary={stubLoadModelSummary}
                                                         relation={relation}
                                                         onChange={stubOnChange} meta={meta} models={models}/>;

  let sandbox;
  beforeEach(() => {
    wrapper = shallow(coreMultiSelect);
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  })

  it("should be rendered successfully", () => {
    expect(wrapper.exists()).to.be.true;
  });

  it("should contain a multiselect box", () => {
    expect(wrapper.find(FormControl).prop("componentClass")).to.equal("Select");
    expect(wrapper.find(FormControl).prop("multiple")).to.be.true;
  });

  it("should contain the correct number of options", () => {
    expect(wrapper.find("option").length).to.equal(2);
  });

  it("on change the value state should be updated", () => {
    wrapper = mount(coreMultiSelect);
    expect(wrapper.state().value).to.not.be.defined;
    wrapper.find(FormControl).simulate("change", mockSelectEvent);

    expect(wrapper.state().value[0]).to.equal(optionOne.id);
  });

  it("on change add is called on relation property", () => {
    let stub = sandbox.stub(relation, "add");
    wrapper = mount(coreMultiSelect);
    wrapper.find(FormControl).simulate("change", mockSelectEvent);

    sinon.assert.calledOnce(stub);
  });

  it("when an option is deselected it is removed from the relation property", () => {
    let stub = sandbox.stub(relation, "remove");
    wrapper = mount(coreMultiSelect);
    wrapper.find(FormControl).simulate("change", mockSelectEvent);
    wrapper.find(FormControl).simulate("change", mockDeselectEvent);

    sinon.assert.calledOnce(stub);
  })
});